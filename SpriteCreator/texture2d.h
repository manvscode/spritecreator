#ifndef _TEXTURE2D_H_
#define _TEXTURE2D_H_

#include <types.h>
#include <ResourceManagement/resource.h>

namespace ResourceManagement {


enum TextureFormat {
	RGB  = 3,
	RGBA = 4 
};


class Texture2D : public Resource
{
  public:
	Texture2D( );
	Texture2D( ui32 textureID, ui32 width, ui32 height, ui32 byteCount, byte *bitmap, bool makeCopy = false );
	Texture2D( const Texture2D &t );
	virtual ~Texture2D( );
	
	ui32 Width( ) const;
	ui32 Height( ) const;
	ui32 ByteCount( ) const;
	ui32 BitCount( ) const;
	ui32 Format( ) const;
	
	byte *Bitmap( );
	const byte *Bitmap( ) const;

	byte &operator[]( ui32 i );
	const byte &operator[]( ui32 i ) const;
	const Texture2D &operator =( const Texture2D &right );

  protected:
	ui32 m_Width, m_Height;
	ui32 m_ByteCount;
	byte *m_Bitmap;
	ui32 m_TextureID;
	bool m_bMemoryAllocated;

	void CleanUp( );
};

inline ui32 Texture2D::Width() const
{ return m_Width; }

inline ui32 Texture2D::Height() const
{ return m_Height; }

inline ui32 Texture2D::ByteCount( ) const
{ return m_ByteCount; }

inline ui32 Texture2D::BitCount( ) const
{ return m_ByteCount * 8; }

inline ui32 Texture2D::Format( ) const
{ return (m_ByteCount == 4 ? TextureFormat::RGBA : TextureFormat::RGB ); }


} // end of namespace
#endif
