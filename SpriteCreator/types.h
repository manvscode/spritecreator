#ifndef __TYPES_H__
#define __TYPES_H__


typedef unsigned int    ui32;
typedef signed int      i32;
typedef unsigned short  us32;
typedef signed short    s32;
typedef unsigned char   uc8;
typedef unsigned char   byte;

#endif // __TYPES_H__
