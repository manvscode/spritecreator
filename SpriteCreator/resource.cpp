#include "resource.h"

namespace ResourceManagement {

Resource::Resource( ui32 id )
	: m_nID(id)
{
} 

Resource::Resource( const Resource &r )
	: m_nID(r.ID())
{
}

Resource &Resource::operator=( const Resource &r )
{
	m_nID = r.ID( );
}

} // end of namespace
