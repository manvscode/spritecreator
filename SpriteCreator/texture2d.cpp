#include <ResourceManagement/TextureManager/Texture2D.h>
#include <cstring>


namespace ResourceManagement {


Texture2D::Texture2D( )
	: Resource(0)
{
	// intentional not initializing.
}

Texture2D::Texture2D( ui32 textureID, ui32 width, ui32 height, ui32 byteCount, byte *bitmap, bool makeCopy )
	: Resource(textureID), m_Width(width), m_Height(height), m_ByteCount(byteCount), m_Bitmap(bitmap), m_bMemoryAllocated(false)
{
	if( makeCopy == true )
	{
		m_Bitmap = new byte[ width * height * byteCount ];
		std::memcpy( m_Bitmap, bitmap, sizeof(byte) * width * height * byteCount );
		m_bMemoryAllocated = true;
	}
}

Texture2D::Texture2D( const Texture2D &t )
	: Resource(t), m_Width(t.m_Width), m_Height(t.m_Height), m_ByteCount(t.m_ByteCount)
{
	m_Bitmap = new byte[ m_Width * m_Height * m_ByteCount ];
	std::memcpy( m_Bitmap, t.m_Bitmap, sizeof(byte) * m_Width * m_Height * m_ByteCount );
	m_bMemoryAllocated = true;
}

Texture2D::~Texture2D( )
{ 
	CleanUp( ); 
}

byte *Texture2D::Bitmap( )
{
	return m_Bitmap;
}

const byte *Texture2D::Bitmap( ) const
{
	return m_Bitmap;
}

byte &Texture2D::operator[]( ui32 i )
{
	return m_Bitmap[ i ];
}

const byte &Texture2D::operator[]( ui32 i ) const
{
	return m_Bitmap[ i ];
}

const Texture2D &Texture2D::operator =( const Texture2D &right )
{
	if( this != &right )
	{
		m_nID       = right.m_nID;
		m_Width     = right.m_Width; 
		m_Height    = right.m_Height;
		m_ByteCount = right.m_ByteCount;


		CleanUp( );
		m_Bitmap           = new byte[ m_Width * m_Height * m_ByteCount ];
		m_bMemoryAllocated = true;
		std::memcpy( m_Bitmap, right.m_Bitmap, sizeof(byte) * m_Width * m_Height * m_ByteCount );
	}

	return *this;
}

void Texture2D::CleanUp( )
{
	if( m_bMemoryAllocated )
	{
		delete [] m_Bitmap;
		m_bMemoryAllocated = false;
	}
}

} // end of namespace
