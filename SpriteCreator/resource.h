#ifndef __RESOURCE_H__
#define __RESOURCE_H__

#include "types.h"

namespace ResourceManagement {

class Resource
{
  protected:
	ui32 m_nID;

  public:
	Resource( ui32 id );
	virtual ~Resource( ) {}
	Resource( const Resource &r );
	Resource &operator=( const Resource &r );
	ui32 ID( ) const;

};

inline ui32 Resource::ID( ) const 
{ return m_nID; }

} // end of namespace

#endif // __RESOURCE_H__
