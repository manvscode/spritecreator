#ifndef _SPRITERESOURCE_H_
#define _SPRITERESOURCE_H_

#include "fileresource.h"

namespace ResourceManagement {

class Sprite //: public FileResource
{
  public:
	typedef struct stImage {
		ui32  nWidth;
		ui32  nHeight;
		us32  nBytesPerPixel;
		byte* pPixels;
	} Image;
	typedef struct stFrame {
		ui32 nX;
		ui32 nY;
		ui32 nWidth;
		ui32 nHeight;
	} Frame;	
	typedef std::vector<Frame> FrameCollection;
	typedef std::map<std::string, FrameCollection> AnimationCollection;

  protected:
	Image image;
	AnimationCollection animations;

	Sprite( );

  public:
	static Sprite *FromXml( const char *xmlFilename );
};


} // end of namespace

#endif // _SPRITERESOURCE_H_
