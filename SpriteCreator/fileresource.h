#ifndef _FILERESOURCE_H_
#define _FILERESOURCE_H_


namespace ResourceManagement {

class FileResource : public Resource
{
  protected:
	FileResource( ui32 id ) : Resource(id) {}

  public:
	static FileResource *FromFile( const char *filename ) = 0;
};


} // end of namespace

#endif // _FILERESOURCE_H_
